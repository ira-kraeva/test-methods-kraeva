package ru.nsu.fit.tests.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;

public class UpdatePlanApiTest extends TestObject {

    private PlanPojo plan;

    @Test(description = "Update plan via API")
    @Features({"API", "Plan"})
    public void updatePlan() {
        plan = new PlanPojo();
        plan.name = "Update plan";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);

        plan.id = plan.id;
        plan.name = "Update new plan";
        plan.details = "Description of new plan";
        plan.fee = 101;

        plan = updatePlanViaApi(plan);

        getExistingPlanViaApi(plan);
    }
}
