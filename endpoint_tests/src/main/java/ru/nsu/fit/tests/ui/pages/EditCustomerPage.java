package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class EditCustomerPage {
    private final Browser browser;

    public EditCustomerPage(Browser browser) {
        this.browser = browser;
    }

    By firstNameLocator = By.id("first_name_id");
    By lastNameLocator = By.id("last_name_id");
    By editCustomerButtonLocator = By.id("update_customer_id");


    public EditCustomerPage changeFirstName(String firstName) {
        browser.getElement(firstNameLocator).clear();
        browser.sendKeys(firstNameLocator, firstName);
        return this;
    }

    public EditCustomerPage changeLastName(String lastName) {
        browser.getElement(lastNameLocator).clear();
        browser.sendKeys(lastNameLocator, lastName);
        return this;
    }

    public CustomersPage editCustomer() {
        browser.waitForElement(editCustomerButtonLocator);
        browser.getElement(editCustomerButtonLocator).click();
        return new CustomersPage(browser);
    }
}
