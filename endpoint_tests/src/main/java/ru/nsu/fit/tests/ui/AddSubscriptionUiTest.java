package ru.nsu.fit.tests.ui;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.TestObject;
import ru.nsu.fit.tests.ui.pages.*;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class AddSubscriptionUiTest extends TestObject {
    private Browser browser = null;

    private PlanPojo plan;
    private CustomerPojo customer;
    private SubscriptionPojo subscription;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Add subscription")
    @Description("User with role CUSTOMER add subscription")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Subscription", "UI"})
    public void addSubscription() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "add_subscription_via_ui@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);

        plan = new PlanPojo();
        plan.name = "Plan";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);

        loginAsCustomer(customer);
        createSubscription();

        subscription = new SubscriptionPojo();
        subscription.planName = plan.name;

        findCreatedSubscriptionInList(subscription);
    }

    @Step("Login as customer")
    private void loginAsCustomer(CustomerPojo customer) {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);

        page.typeEmail(customer.login);
        page.typePassword(customer.pass);
        page.loginAsCustomer();
    }

    @Step ("Add subscription")
    private void createSubscription() {
        SubscriptionsPage subscriptionsPage = new SubscriptionsPage(browser);
        subscriptionsPage.createSubscription();

        AddSubscriptionPage addSubscriptionPage = new AddSubscriptionPage(browser);
        addSubscriptionPage.selectPlanName("Plan");
        addSubscriptionPage.createSubscription();
    }

    @Step("Find created subscription in list")
    private void findCreatedSubscriptionInList(SubscriptionPojo subscription) {
        SubscriptionsPage page = new SubscriptionsPage(browser);
        Assert.assertTrue(page.findSubscriptionByValue(subscription));
    }
}