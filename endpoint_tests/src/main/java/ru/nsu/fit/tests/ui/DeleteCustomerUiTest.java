package ru.nsu.fit.tests.ui;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.TestObject;
import ru.nsu.fit.tests.ui.pages.CustomersPage;
import ru.nsu.fit.tests.ui.pages.LoginPage;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class DeleteCustomerUiTest extends TestObject {
    private Browser browser = null;
    private CustomerPojo customer;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Delete customer")
    @Description("User with role ADMIN deletes customer")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Customer", "UI"})
    public void deleteCustomer() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "delete_customer_via_ui@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);
        loginAsAdmin();
        deleteCreatedCustomer(customer);
    }

    @Step("Login as admin")
    private void loginAsAdmin() {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail("admin");
        page.typePassword("password");
        page.loginAsAdmin();
    }

    @Step("Delete customer")
    private void deleteCreatedCustomer(CustomerPojo customer) {
        CustomersPage customerPage = new CustomersPage(browser);
        customerPage.selectCustomerByValue(customer);
        customerPage.deleteCustomer();
    }
}
