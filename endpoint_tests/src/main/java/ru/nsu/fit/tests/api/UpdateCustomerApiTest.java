package ru.nsu.fit.tests.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;

public class UpdateCustomerApiTest extends TestObject {
    CustomerPojo customer;

    @Test(description = "Update customer via API")
    @Features({"API", "Customer"})
    public void updateCustomer() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "update_customer_via_api@login.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);

        customer.setId(customer.id);
        customer.setFirstName("Newcustomer");
        customer.setLastName("Newcustomer");
        customer.setLogin("update_customer_via_api@login.com");
        customer.setPass("password");

        updateCustomerViaApi(customer);
        getExistingCustomerViaApi(customer);
    }
}
