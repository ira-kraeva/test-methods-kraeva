package ru.nsu.fit.tests.ui;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.TestObject;
import ru.nsu.fit.tests.ui.pages.CustomersPage;
import ru.nsu.fit.tests.ui.pages.EditCustomerPage;
import ru.nsu.fit.tests.ui.pages.LoginPage;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class EditCustomerUiTest extends TestObject {
    private Browser browser = null;
    private CustomerPojo customer;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Edit customer")
    @Description("User with role ADMIN edits customer")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Customer", "UI"})
    public void editCustomer() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "edit_customer_via_ui@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);
        loginAsAdmin();

        customer.firstName = "Ivan";
        customer.lastName = "Slabiy";

        editCreatedCustomer(customer);
        getExistingCustomerViaApi(customer);
    }

    @Step("Login as admin")
    private void loginAsAdmin() {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail("admin");
        page.typePassword("password");
        page.loginAsAdmin();
    }

    @Step("Edit customer")
    private void editCreatedCustomer(CustomerPojo customer) {
        CustomersPage customerPage = new CustomersPage(browser);
        customerPage.selectCustomerByValue(customer);
        customerPage.editCustomer();

        EditCustomerPage editCustomerPage = new EditCustomerPage(browser);
        editCustomerPage.changeFirstName(customer.firstName);
        editCustomerPage.changeLastName(customer.lastName);
        editCustomerPage.editCustomer();
    }
}
