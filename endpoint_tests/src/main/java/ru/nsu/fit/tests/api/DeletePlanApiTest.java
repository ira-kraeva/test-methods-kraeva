package ru.nsu.fit.tests.api;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class DeletePlanApiTest extends TestObject {
    private PlanPojo plan;

    @Severity(SeverityLevel.NORMAL)
    @Features({"API", "Plan"})
    @Test(description = "Delete plan via API")
    public void deletePlan() {
        plan = new PlanPojo();
        plan.name = "Delete plan";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);
        deletePlanViaApi(plan);
        getNonexistentPlanViaApi(plan);
    }
}
