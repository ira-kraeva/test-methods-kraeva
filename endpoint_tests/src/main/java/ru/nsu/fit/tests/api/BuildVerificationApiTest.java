package ru.nsu.fit.tests.api;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.shared.Globals;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;


import javax.ws.rs.core.Response;

public class BuildVerificationApiTest extends TestObject {

    @Severity(SeverityLevel.BLOCKER)
    @Features({"API", "Maintenance"})
    @Test(description = "Health check")
    public void healthCheck() {
        Response response = apiService.get("health_check", Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }
}
