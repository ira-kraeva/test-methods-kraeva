package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class CreateCustomerPage {
    private final Browser browser;

    public CreateCustomerPage(Browser browser) {
        this.browser = browser;
    }

    By firstNameLocator = By.id("first_name_id");
    By lastNameLocator = By.id("last_name_id");
    By emailLocator = By.id("email_id");
    By passwordLocator = By.id("password_id");
    By createCustomerButtonLocator = By.id("create_customer_id");

    public CreateCustomerPage typeFirstName(String firstName) {
        browser.waitForElement(firstNameLocator);
        browser.getElement(firstNameLocator).sendKeys(firstName);
        return this;
    }

    public CreateCustomerPage typeLastName(String lastName) {
        browser.getElement(lastNameLocator).sendKeys(lastName);
        return this;
    }

    public CreateCustomerPage typeEmail(String email) {
        browser.sendKeys(emailLocator, email);
        return this;
    }

    public CreateCustomerPage typePassword(String password) {
        browser.sendKeys(passwordLocator, password);
        return this;
    }

    public CustomersPage createCustomer() {
        browser.waitForElement(createCustomerButtonLocator);
        browser.getElement(createCustomerButtonLocator).click();
        return new CustomersPage(browser);
    }
}
