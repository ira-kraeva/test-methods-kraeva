package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.*;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.RolePojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.shared.Globals;
import ru.nsu.fit.services.api.ApiTestService;
import ru.yandex.qatools.allure.annotations.Step;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

public abstract class TestObject {
    protected ApiTestService apiService;
    protected IDBService dbService;

    @BeforeSuite
    public void beforeSuite() {
        dbService = DBService.getInstance();

        dbService.deleteAllCustomers();
        dbService.deleteAllPlans();
        dbService.deleteAllSubscriptions();
    }

    @BeforeClass
    public void beforeTestObjectClass() {
        apiService = new ApiTestService();
        dbService = DBService.getInstance();
    }

    @AfterSuite
    private void afterSuite() {
        dbService = DBService.getInstance();

        dbService.deleteAllCustomers();
        dbService.deleteAllPlans();
        dbService.deleteAllSubscriptions();
    }

    @Step("Get existing customer via API")
    protected void getExistingCustomerViaApi(CustomerPojo customer) {
        Response response = apiService.get("get_customer_id/" + customer.login,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        CustomerPojo receivedCustomer = response.readEntity(CustomerPojo.class);
        Assert.assertEquals(receivedCustomer.firstName, customer.firstName);
        Assert.assertEquals(receivedCustomer.lastName, customer.lastName);
        Assert.assertEquals(receivedCustomer.login, customer.login);
        Assert.assertEquals(receivedCustomer.pass, customer.pass);
        Assert.assertEquals(receivedCustomer.balance, customer.balance);
    }

    @Step("Get nonexistent customer via API")
    protected void getNonexistentCustomerViaApi(CustomerPojo customer) {
        Response response = apiService.get("get_customer_id/" + customer.login,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.BAD_REQUEST);
    }

    @Step("Find customer in list via API")
    protected void findCustomerInList(CustomerPojo customer) {
        Response response = apiService.get("get_customers", Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        List<CustomerPojo> customers = response.readEntity(new GenericType<List<CustomerPojo>>() {
        });
        Assert.assertTrue(customers.contains(customer));
    }

    @Step("Create customer via API")
    protected CustomerPojo createCustomerViaApi(CustomerPojo customer) {
        Response response = apiService.post("create_customer", customer,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        CustomerPojo createdCustomer = response.readEntity(CustomerPojo.class);
        Assert.assertEquals(createdCustomer.firstName, customer.firstName);
        Assert.assertEquals(createdCustomer.lastName, customer.lastName);
        Assert.assertEquals(createdCustomer.login, customer.login);
        Assert.assertEquals(createdCustomer.pass, customer.pass);
        Assert.assertEquals(createdCustomer.balance, customer.balance);

        return createdCustomer;
    }

    @Step("Update customer via API")
    protected void updateCustomerViaApi(CustomerPojo customer) {
        Response updateResponse = apiService.post("update_customer", customer, Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(updateResponse.getStatusInfo(), Response.Status.OK);

        CustomerPojo updatedCustomer = updateResponse.readEntity(CustomerPojo.class);
        Assert.assertEquals(updatedCustomer.firstName, customer.firstName);
        Assert.assertEquals(updatedCustomer.lastName, customer.lastName);
        Assert.assertEquals(updatedCustomer.login, customer.login);
        Assert.assertEquals(updatedCustomer.pass, customer.pass);
        Assert.assertEquals(updatedCustomer.balance, customer.balance);
    }

    @Step("Delete customer via API")
    protected void deleteCustomerViaApi(CustomerPojo customer) {
        Response removeResponse = apiService.delete("/delete_customer/" + customer.login,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(removeResponse.getStatusInfo(), Response.Status.OK);
    }

    @Step("Get existing plan via API")
    protected void getExistingPlanViaApi(PlanPojo plan) {
        Response response = apiService.get("get_plan_id/" + plan.id,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        PlanPojo receivedPlan = response.readEntity(PlanPojo.class);
        Assert.assertEquals(receivedPlan.name, plan.name);
        Assert.assertEquals(receivedPlan.details,plan.details);
        Assert.assertEquals(receivedPlan.fee, plan.fee);
    }

    @Step("Get nonexistent plan via API")
    protected void getNonexistentPlanViaApi(PlanPojo plan) {
        Response response = apiService.get("get_plan_id/" + plan.id,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.BAD_REQUEST);
    }

    @Step("Find plan in list via API")
    protected void findPlanInListViaApi(PlanPojo plan) {
        Response response = apiService.get("get_plans", Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        List<PlanPojo> plans = response.readEntity(new GenericType<List<PlanPojo>>() {});
        Assert.assertTrue(plans.contains(plan));
    }

    @Step("Create plan via API")
    protected PlanPojo createPlanViaApi(PlanPojo plan) {
        Response response = apiService.post("create_plan", plan,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        plan = response.readEntity(PlanPojo.class);

        return plan;
    }

    @Step("Update plan via API")
    protected PlanPojo updatePlanViaApi(PlanPojo plan) {

        Response updateResponse = apiService.post("update_plan/" + plan.id.toString(), plan,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(updateResponse.getStatusInfo(), Response.Status.OK);

        PlanPojo updatedPlan = updateResponse.readEntity(PlanPojo.class);
        Assert.assertEquals(updatedPlan.name, plan.name);
        Assert.assertEquals(updatedPlan.details, plan.details);
        Assert.assertEquals(updatedPlan.fee, plan.fee);

        return plan;
    }

    @Step("Delete plan via API")
    protected void deletePlanViaApi(PlanPojo plan) {
        Response response = apiService.delete("/delete_plan/" + plan.id, Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Step("Find existing subscription in list of customer's subscriptions via API")
    protected void findExistingSubscriptionInListViaApi(SubscriptionPojo subscription) {
        Response response = apiService.get("get_customer_subscriptions/" + subscription.customerId,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        List<SubscriptionPojo> subscriptions = response.readEntity(new GenericType<List<SubscriptionPojo>>() {
        });
        Assert.assertTrue(subscriptions.contains(subscription));
    }

    @Step("Find nonexistent subscription in list of customer's subscriptions via API")
    protected void findNonexistentSubscriptionInListViaApi(SubscriptionPojo subscription) {
        Response response = apiService.get("get_customer_subscriptions/" + subscription.customerId,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        List<SubscriptionPojo> subscriptions = response.readEntity(new GenericType<List<SubscriptionPojo>>() {
        });
        Assert.assertFalse(subscriptions.contains(subscription));
    }

    @Step("Create subscription via API")
    protected SubscriptionPojo createSubscriptionViaApi(SubscriptionPojo subscription) {
        Response createSubscriptionResponse = apiService.post("create_subscription", subscription,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(createSubscriptionResponse.getStatusInfo(), Response.Status.OK);

        SubscriptionPojo createdSubscription = createSubscriptionResponse.
                readEntity(SubscriptionPojo.class);

        Assert.assertEquals(createdSubscription.customerId, subscription.customerId);
        Assert.assertEquals(createdSubscription.planId, subscription.planId);

        return createdSubscription;
    }

    @Step("Delete subscription via API")
    protected void deleteSubscriptionViaApi(SubscriptionPojo subscription) {
        Response deleteSubscriptionResponse = apiService.delete("delete_subscription/"
                        + subscription.id,
                Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(deleteSubscriptionResponse.getStatusInfo(), Response.Status.OK);
    }

    @Step("Get admin role via API")
    protected void getAdminRole() {
        Response response = apiService.get("get_role", Globals.ADMIN_LOGIN, Globals.ADMIN_PASS);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        RolePojo role = response.readEntity(RolePojo.class);
        RolePojo expectedRole = new RolePojo().setRole("ADMIN");
        Assert.assertEquals(role.getRole(), expectedRole.getRole());
    }

    @Step("Get customer role via API")
    protected void getCustomerRole(CustomerPojo customer) {
        Response getRoleResponse = apiService.get("get_role", customer.login, customer.pass);
        Assert.assertEquals(getRoleResponse.getStatusInfo(), Response.Status.OK);

        RolePojo role = getRoleResponse.readEntity(RolePojo.class);
        RolePojo expectedRole = new RolePojo().setRole("CUSTOMER");
        Assert.assertEquals(role.getRole(), expectedRole.getRole());
    }

    @Step("Get unknown role via API")
    protected void getUnknownRole() {
        Response response = apiService.get("get_role", "unknown_user", "password");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        RolePojo role = response.readEntity(RolePojo.class);
        RolePojo expectedRole = new RolePojo().setRole("UNKNOWN");
        Assert.assertEquals(role.getRole(), expectedRole.getRole());
    }
}
