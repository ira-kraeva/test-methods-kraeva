package ru.nsu.fit.tests.api;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class DeleteSubscriptionApiTest extends TestObject {
    private CustomerPojo customer;
    private PlanPojo plan;
    private SubscriptionPojo subscription;

    @Severity(SeverityLevel.BLOCKER)
    @Features({"API", "Subscription"})
    @Test(description = "Create subscription via API")
    public void deleteSubscription() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "delete_subscription_via_api@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);

        customer.balance = 1000;

        updateCustomerViaApi(customer);

        plan = new PlanPojo();
        plan.name = "Delete subscription";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);

        subscription = new SubscriptionPojo();
        subscription.customerId = customer.id;
        subscription.planId = plan.id;

        subscription = createSubscriptionViaApi(subscription);

        deleteSubscriptionViaApi(subscription);
        findNonexistentSubscriptionInListViaApi(subscription);
    }
}
