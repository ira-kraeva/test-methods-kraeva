package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import ru.nsu.fit.services.browser.Browser;

public class AddSubscriptionPage {
    private final Browser browser;

    public AddSubscriptionPage(Browser browser) {
        this.browser = browser;
    }

    By planNameLocator = By.id("plan_name_id");
    By addSubscriptioButtonLocator = By.id("create_subscription_id");

    public AddSubscriptionPage selectPlanName(String planName) {
        browser.waitForElement(planNameLocator);
        Select planNames = new Select(browser.getElement(planNameLocator));
        planNames.selectByVisibleText(planName);
        return new AddSubscriptionPage(browser);
    }

    public SubscriptionsPage createSubscription() {
        browser.waitForElement(addSubscriptioButtonLocator);
        browser.getElement(addSubscriptioButtonLocator).click();
        return new SubscriptionsPage(browser);
    }
}
