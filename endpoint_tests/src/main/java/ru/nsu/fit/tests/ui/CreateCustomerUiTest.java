package ru.nsu.fit.tests.ui;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.TestObject;
import ru.nsu.fit.tests.ui.pages.CreateCustomerPage;
import ru.nsu.fit.tests.ui.pages.CustomersPage;
import ru.nsu.fit.tests.ui.pages.LoginPage;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreateCustomerUiTest extends TestObject {
    private Browser browser = null;
    private CustomerPojo customer;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Create customer")
    @Description("User with role ADMIN creates customer")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Customer", "UI"})
    public void createCustomer() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "create_customer_via_ui@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        loginAsAdmin();
        addNewCustomer(customer);
        findCreatedCustomerInList(customer);
        getExistingCustomerViaApi(customer);
        loginAsCustomer(customer);
    }

    @Step ("Login as admin")
    private void loginAsAdmin() {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail("admin");
        page.typePassword("password");
        page.loginAsAdmin();
    }

    @Step ("Create customer")
    private void addNewCustomer(CustomerPojo customer) {
        CustomersPage customerPage = new CustomersPage(browser);
        customerPage.createCustomer();

        CreateCustomerPage createCustomerPage = new CreateCustomerPage(browser);
        createCustomerPage.typeFirstName(customer.firstName);
        createCustomerPage.typeLastName(customer.lastName);
        createCustomerPage.typeEmail(customer.login);
        createCustomerPage.typePassword(customer.pass);
        createCustomerPage.createCustomer();
    }

    @Step("Find created customer in list")
    private void findCreatedCustomerInList(CustomerPojo customer) {
        CustomersPage page = new CustomersPage(browser);
        Assert.assertTrue(page.findCustomerByValue(customer));
    }

    @Step("Login by created customer")
    private void loginAsCustomer(CustomerPojo customer) {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail(customer.login);
        page.typePassword(customer.pass);
        page.loginAsCustomer();
    }


}
