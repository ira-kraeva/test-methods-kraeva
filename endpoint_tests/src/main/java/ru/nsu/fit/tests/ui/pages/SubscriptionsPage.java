package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.services.browser.Browser;

import java.util.UUID;

public class SubscriptionsPage {
    private final Browser browser;

    public SubscriptionsPage(Browser browser) {
        this.browser = browser;
    }

    By addSubscriptionButtonLocator = By.id("add_new_subscription");
    By deleteSubscriptionButtonLocator = By.id("delete_subscription");
    By subscriptionTableLocator = By.id("subscription_list_id");


    public AddSubscriptionPage createSubscription() {
        browser.waitForElement(addSubscriptionButtonLocator);
        browser.getElement(addSubscriptionButtonLocator).click();
        return new AddSubscriptionPage(browser);
    }

    public SubscriptionsPage selectSubscriptionByValue(SubscriptionPojo subscription) {
        browser.selectRowInTable(subscriptionTableLocator, subscription.planName);
        return this;
    }

    public Boolean findSubscriptionByValue(SubscriptionPojo subscription) {
        Boolean subscriptionIsFound = browser.findRowInTable(subscriptionTableLocator, subscription.planName);
        return subscriptionIsFound;
    }


    public SubscriptionsPage deleteSubscription() {
        browser.waitForElement(deleteSubscriptionButtonLocator);
        browser.getElement(deleteSubscriptionButtonLocator).click();
        return this;
    }
}
