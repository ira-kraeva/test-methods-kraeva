package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

public class LoginPage {
    private final Browser browser;

    public LoginPage(Browser browser) {
        this.browser = browser;
    }

    By emailLocator = By.id("email");
    By passwordLocator = By.id("password");
    By loginButtonLocator = By.id("login");

    public LoginPage typeEmail(String email) {
        browser.waitForElement(emailLocator);
        browser.sendKeys(emailLocator, email);

        AllureUtils.saveImageAttach("Email is typed", browser.makeScreenshot());
        AllureUtils.saveTextLog("Email is typed: " + email);

        return this;
    }

    public LoginPage typePassword(String password) {
        browser.waitForElement(passwordLocator);
        browser.sendKeys(passwordLocator, password);
        return this;
    }

    public CustomersPage loginAsAdmin() {
        browser.getElement(loginButtonLocator).click();
        return new CustomersPage(browser);
    }

    public SubscriptionsPage loginAsCustomer() {
        browser.getElement(loginButtonLocator).click();
        return new SubscriptionsPage(browser);
    }

    public LoginPage loginFailed() {
        Boolean result = browser.isAlertPresent(loginButtonLocator, "Email or password is incorrect");
        Assert.assertTrue(result);
        return this;
    }
}
