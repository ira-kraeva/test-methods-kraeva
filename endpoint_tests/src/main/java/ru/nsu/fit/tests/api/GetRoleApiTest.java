package ru.nsu.fit.tests.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class GetRoleApiTest extends TestObject {
    CustomerPojo customer;

    @Severity(SeverityLevel.BLOCKER)
    @Features({"API", "Access control"})
    @Test(description = "Create subscription via API")
    public void getRole(){
        getAdminRole();

        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "get_role_via_api@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        createCustomerViaApi(customer);
        getCustomerRole(customer);
        getUnknownRole();
    }
}
