package ru.nsu.fit.tests.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreatePlanApiTest extends TestObject {
    private PlanPojo plan;

    @Severity(SeverityLevel.BLOCKER)
    @Features({"API", "Plan"})
    @Test(description = "Create plan via API")
    public void createPlan() {
        plan = new PlanPojo();
        plan.name = "Create plan";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);
        getExistingPlanViaApi(plan);
        findPlanInListViaApi(plan);
    }
}
