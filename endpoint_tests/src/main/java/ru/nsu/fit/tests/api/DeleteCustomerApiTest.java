package ru.nsu.fit.tests.api;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class DeleteCustomerApiTest extends TestObject {

    @Severity(SeverityLevel.NORMAL)
    @Features({"API", "Customer"})
    @Test(description = "Delete customer via API")
    public void deleteCustomer() {
        CustomerPojo customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "delete_customer_via_api@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        createCustomerViaApi(customer);
        deleteCustomerViaApi(customer);
        getNonexistentCustomerViaApi(customer);
    }
}
