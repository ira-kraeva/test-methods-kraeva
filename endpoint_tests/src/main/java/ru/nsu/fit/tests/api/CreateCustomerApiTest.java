package ru.nsu.fit.tests.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.tests.TestObject;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreateCustomerApiTest extends TestObject {

    private CustomerPojo customer;

    @Severity(SeverityLevel.BLOCKER)
    @Features({"API", "Customer"})
    @Test(description = "Create customer via API.")
    public void createCustomer() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "create_customer_via_api@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);
        getExistingCustomerViaApi(customer);
        findCustomerInList(customer);
    }
}