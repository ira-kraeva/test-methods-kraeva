package ru.nsu.fit.tests.ui;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.pages.LoginPage;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.annotation.concurrent.NotThreadSafe;

@NotThreadSafe
public class LoginFailedUiTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Login failed")
    @Description("User logins with wrong password")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Access control", "UI"})
    public void loginFailed() {
        loginWithWrongPassword();
    }

    @Step("Login with wrong password")
    private void loginWithWrongPassword() {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail("admin");
        page.typePassword("password1");
        page.loginFailed();
    }
}

