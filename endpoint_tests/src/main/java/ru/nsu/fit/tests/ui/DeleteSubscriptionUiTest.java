package ru.nsu.fit.tests.ui;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.TestObject;
import ru.nsu.fit.tests.ui.pages.LoginPage;
import ru.nsu.fit.tests.ui.pages.SubscriptionsPage;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class DeleteSubscriptionUiTest extends TestObject {

    private Browser browser = null;
    private CustomerPojo customer;
    private PlanPojo plan;
    private SubscriptionPojo subscription;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("Delete subscription")
    @Description("User with role CUSTOMER delete subscription")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Subscription", "UI"})
    public void deleteSubscription() {
        customer = new CustomerPojo();
        customer.firstName = "Customer";
        customer.lastName = "Customer";
        customer.login = "delete_subscription_via_ui@mail.com";
        customer.pass = "password";
        customer.balance = 0;

        customer = createCustomerViaApi(customer);

        plan = new PlanPojo();
        plan.name = "Plan";
        plan.details = "Description of plan";
        plan.fee = 100;

        plan = createPlanViaApi(plan);

        subscription = new SubscriptionPojo();
        subscription.customerId = customer.id;
        subscription.planId = plan.id;
        subscription.planName = plan.name;

        createSubscriptionViaApi(subscription);
        loginAsCustomer(customer);
        deleteSubscription(subscription);
        findDeletedSubscriptionInList(subscription);
        findNonexistentSubscriptionInListViaApi(subscription);
    }

    @Step("Login as customer")
    private void loginAsCustomer(CustomerPojo customer) {
        browser.openPage("http://localhost:8080/endpoint");

        LoginPage page = new LoginPage(browser);
        page.typeEmail(customer.login);
        page.typePassword(customer.pass);
        page.loginAsCustomer();
    }

    @Step("Delete subscription")
    private void deleteSubscription(SubscriptionPojo subscription) {
        SubscriptionsPage subscriptionsPage = new SubscriptionsPage(browser);
        subscriptionsPage.selectSubscriptionByValue(subscription);
        subscriptionsPage.deleteSubscription();
    }

    @Step("Find deleted subscription in list")
    private void findDeletedSubscriptionInList(SubscriptionPojo subscription) {
        SubscriptionsPage page = new SubscriptionsPage(browser);
        Assert.assertFalse(page.findSubscriptionByValue(subscription));
    }
}