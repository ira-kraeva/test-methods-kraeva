package ru.nsu.fit.tests.ui.pages;

import org.openqa.selenium.By;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.browser.Browser;

public class CustomersPage {
    private final Browser browser;

    public CustomersPage(Browser browser) {
        this.browser = browser;
    }

    By addCustomerButtonLocator = By.id("add_new_customer");
    By editCustomerButtonLocator = By.id("update_customer");
    By deleteCustomerButtonLocator = By.id("delete_customer");
    By customersTableLocator = By.id("customer_list_id");
//    By customersTableRowsLocator = By.xpath(".//*[@id='customer_list_id']/tbody/tr");
//    By customerLoginLocator = By.xpath("/td[3]");


    public CreateCustomerPage createCustomer() {
        browser.waitForElement(addCustomerButtonLocator);
        browser.getElement(addCustomerButtonLocator).click();
        return new CreateCustomerPage(browser);
    }

    public EditCustomerPage editCustomer() {
        browser.waitForElement(editCustomerButtonLocator);
        browser.getElement(editCustomerButtonLocator).click();
        return new EditCustomerPage(browser);
    }

    public CustomersPage deleteCustomer() {
        browser.waitForElement(deleteCustomerButtonLocator);
        browser.getElement(deleteCustomerButtonLocator).click();
        return this;
    }

    public CustomersPage selectCustomerByValue(CustomerPojo customer) {
        browser.selectRowInTable(customersTableLocator, customer.login);
        return this;
    }

    public Boolean findCustomerByValue(CustomerPojo customer) {
        Boolean customerIsFound = browser.findRowInTable(customersTableLocator, customer.login);
        return customerIsFound;
    }

}
