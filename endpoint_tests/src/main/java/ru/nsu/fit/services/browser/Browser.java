package ru.nsu.fit.services.browser;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.ImageUtils;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Please read: https://github.com/SeleniumHQ/selenium/wiki/Grid2
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Browser implements Closeable {
    private WebDriver webDriver;

    public Browser() {
        // create web driver
        try {
            ChromeOptions chromeOptions = new ChromeOptions();

            // for running in Docker container as 'root'.
            chromeOptions.addArguments("no-sandbox");
            chromeOptions.addArguments("disable-dev-shm-usage");
            chromeOptions.addArguments("disable-setuid-sandbox");
            chromeOptions.addArguments("disable-infobars");

            chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

            // we use Windows platform for development only and not for AT launch.
            // For launch AT regression, we use Linux platform.
            if (System.getProperty("os.name").toLowerCase().contains("win")) {
                System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
                chromeOptions.setHeadless(Boolean.parseBoolean(System.getProperty("headless")));
                webDriver = new ChromeDriver(chromeOptions);
            } else {
                File f = new File("/usr/bin/chromedriver");
                if (f.exists()) {
                    chromeOptions.addArguments("single-process");
                    chromeOptions.addArguments("headless");
                    System.setProperty("webdriver.chrome.driver", f.getPath());
                    webDriver = new ChromeDriver(chromeOptions);
                }
            }

            if (webDriver == null) {
                throw new RuntimeException();
            }

            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public Browser openPage(String url) {
        webDriver.get(url);
        AllureUtils.saveImageAttach("Page with URL " + url + "is open", makeScreenshot());
        AllureUtils.saveTextLog("Page with URL " + url + "is open");
        return this;
    }

    public Browser waitForElement(By element) {
        AllureUtils.saveImageAttach("Element " + element + "is waiting", makeScreenshot());
        AllureUtils.saveTextLog("Element " + element + " is waiting");
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        AllureUtils.saveImageAttach("Element " + element + " is waiting", makeScreenshot());
        AllureUtils.saveTextLog("Element " + element + " is waiting");
        return this;
    }

    public Browser click(By element) {
        webDriver.findElement(element).click();
        AllureUtils.saveImageAttach("Element " + element + " is clicked", makeScreenshot());
        AllureUtils.saveTextLog("Element " + element + " is clicked");
        return this;
    }

    public Boolean isAlertPresent(By element, String text) {
        webDriver.findElement(element).click();

        Alert alert = webDriver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        AllureUtils.saveTextLog("Alert is shown with text: " + alertText);

        return alertText.equals(text);
    }

    public WebElement getElement(By element) {
        AllureUtils.saveTextLog("Element " + element + " is gotten");
        return webDriver.findElement(element);
    }

    public List<WebElement> getElements(By element) {
        AllureUtils.saveImageAttach("Text " + element + " is gotten" + element, makeScreenshot());
        AllureUtils.saveTextLog("Element" + element + " is gotten");
        return webDriver.findElements(element);
    }

    public Browser sendKeys(By element, String value) {
        webDriver.findElement(element).sendKeys(value);
        AllureUtils.saveImageAttach("Text " + value + " was typed in element " + element, makeScreenshot());
        AllureUtils.saveTextLog("Text " + value + " was typed in element " + element);
        return this;
    }

    public Browser selectRowInTable(By tableLocator, String value) {
        WebElement table = webDriver.findElement(tableLocator);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        Boolean rowIsFound = false;

        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.tagName("td"));
            for (WebElement col : cols) {
                AllureUtils.saveTextLog("Value in cell " + col.getText() +", expected value " + value);

                if (value.equals(col.getText())) {
                    rowIsFound = true;
                    row.click();
                    AllureUtils.saveImageAttach("Row in table " + tableLocator + "with value "
                            + value + "is selected", makeScreenshot());
                }
            }
        }

        if (!rowIsFound) {
            AllureUtils.saveImageAttach("Row in table " + tableLocator + "with value "
                    + value + "is not found", makeScreenshot());
        }

        return this;
    }

    public Boolean findRowInTable(By tableLocator, String value) {
        WebElement table = webDriver.findElement(tableLocator);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        Boolean rowIsFound = false;

        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.tagName("td"));
            for (WebElement col : cols) {
                AllureUtils.saveTextLog("Value in cell " + col.getText() +", expected value " + value);

                if (value.equals(col.getText())) {
                    System.out.print(col.getText() + "\t");
                    rowIsFound = true;
                    AllureUtils.saveImageAttach("Row in table " + tableLocator + "with value "
                            + value + "is found", makeScreenshot());
                }
            }
        }

        if (!rowIsFound) {
            AllureUtils.saveImageAttach("Row in table " + tableLocator + "with value "
                    + value + "is not found", makeScreenshot());
        }

        return rowIsFound;
    }

    public byte[] makeScreenshot() {
        try {
            AllureUtils.saveTextLog("Screenshot is taken");
            return ImageUtils.toByteArray(((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE));
        } catch (IOException ex) {
            AllureUtils.saveTextLog("Exception happened during making screenshot");
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() {
        webDriver.close();
        AllureUtils.saveTextLog("Browser is closed");
    }
}
