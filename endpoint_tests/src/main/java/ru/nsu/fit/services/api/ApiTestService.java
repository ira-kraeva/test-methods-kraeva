package ru.nsu.fit.services.api;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class ApiTestService {
    private static final String endpoint = "http://localhost:8080/endpoint/rest";

    public Response post(String path, Object body, String username, String password) {
        Response response = getInvocationBuilder(path, null, username, password).post(Entity.entity(body, MediaType.APPLICATION_JSON));
        response.bufferEntity();

        Logger.debug("\nPOST " + path + "\nbody: " + body.toString() + "\nresponse: " + response.readEntity(String.class));
        return response;
    }

    public Response get(String path, String username, String password) {
        Response response = getInvocationBuilder(path, null, username, password ).get();
        response.bufferEntity();

        Logger.debug("\nGET " + path + "\nresponse: " + response.readEntity(String.class));
        return response;
    }

    public Response delete(String path, String username, String password) {
        Response response = getInvocationBuilder(path, null, username, password).delete();
        response.bufferEntity();

        Logger.debug("\nDELETE " + path + "\nresponse: " + response.readEntity(String.class));
        return response;
    }

    private Invocation.Builder getInvocationBuilder(String path, Map<String, Object> params, String username, String password) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(JacksonFeature.class);
        clientConfig.register(HttpAuthenticationFeature.basic(username, password));

        WebTarget webTarget = ClientBuilder.newClient(clientConfig)
                .target(endpoint)
                .path(path);
        if (params != null) {
            params.forEach(webTarget::queryParam);
        }
        return webTarget.request(MediaType.APPLICATION_JSON);
    }
}

