package ru.nsu.fit.services.browser;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.ImageUtils;

import java.io.IOException;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BrowserService extends Browser{
    public static final String DRIVER_LOCATION = "D:\\WebDriver\\chromedriver.exe";
    protected Logger logger = Logger.getLogger(getClass());
    private final Browser browser = null;

    public void setUp() throws Exception {
        BasicConfigurator.configure();
        System.setProperty("webdriver.chrome.driver", DRIVER_LOCATION);
        System.setProperty("selenide.browser", "chrome");
        System.setProperty("selenide.start-maximized", "false");
       /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        WebDriverRunner.setWebDriver(new ChromeDriver(options));*/
    }

    public static Browser openNewBrowser() {
        return new Browser();
    }
}
