package ru.nsu.fit.endpoint.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerPojo {
    @JsonProperty("id")
    public UUID id;

    @JsonProperty("firstName")
    public String firstName;

    @JsonProperty("lastName")
    public String lastName;

    @JsonProperty("login")
    public String login;

    @JsonProperty("pass")
    public String pass;

    @JsonProperty("balance")
    public int balance;


    public CustomerPojo setId(UUID id) {
        this.id = id;
        return this;
    }

    public CustomerPojo setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerPojo setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerPojo setLogin(String login) {
        this.login = login;
        return this;
    }

    public CustomerPojo setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public CustomerPojo setBalance(int balance) {
        this.balance = balance;
        return this;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", pass='" + pass + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public CustomerPojo clone() {
        return new CustomerPojo()
                .setId(id)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setLogin(login)
                .setPass(pass)
                .setBalance(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerPojo customer = (CustomerPojo) o;

        return id != null ? id.equals(customer.id) : customer.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
