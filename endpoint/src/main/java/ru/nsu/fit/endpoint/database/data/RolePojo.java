package ru.nsu.fit.endpoint.database.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RolePojo {
    @JsonProperty("role")
    public String role;

    public String getRole() {
        return role;
    }

    public RolePojo setRole(String role) {
        this.role = role;
        return this;
    }
}
