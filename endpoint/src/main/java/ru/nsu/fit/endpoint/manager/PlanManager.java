package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PlanManager extends ParentManager {

    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    private static final String IS_NULL_ERROR_MESSAGE = "%s is null";
    private static final String HAS_WRONG_FORMAT_ERROR_MESSAGE = "%s has wrong format: %s";

    static final String NAME_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "Name");
    private static final Pattern NAME_PATTERN = Pattern.compile("^([A-Za-z0-9\\s]+){2,128}$");
    static final String NAME_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "Name", NAME_PATTERN.pattern());


    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
     * /* details - длина не больше 1024 символов и не меньше 1 включительно;
     * /* fee - больше либо равно 0 но меньше либо равно 999999.
     */

    public PlanPojo createPlan(PlanPojo plan) {
        // name не null
        Validate.isTrue(Objects.nonNull(plan.name), NAME_IS_NULL_ERROR);

        // name валидно
        Validate.isTrue(NAME_PATTERN.matcher(plan.name).matches(), NAME_ERROR);


        return dbService.createPlan(plan);
    }

    public PlanPojo updatePlan(PlanPojo plan) {
        return dbService.updatePlan(plan);
    }

    public void deletePlan(UUID id) {
        dbService.deletePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */

    public List<PlanPojo> getPlans(UUID customerId) {
        CustomerPojo customer = dbService.getCustomer(customerId);
        return dbService.getPlans().stream()
                .filter(plan -> plan.fee <= customer.balance)
                .collect(Collectors.toList());
    }

    public List<PlanPojo> getPlans() {
        return dbService.getPlans();
    }

}
