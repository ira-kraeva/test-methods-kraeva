package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.MainFactory;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.HealthCheckPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.shared.Globals;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import org.apache.commons.lang.Validate;

import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("")
public class RestService {
    @RolesAllowed({Globals.ADMIN_ROLE, Globals.UNKNOWN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity(JsonMapper.toJson(new HealthCheckPojo(), true)).build();
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.UNKNOWN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.UNKNOWN_ROLE})
    @GET
    @Path("/get_customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomers(@DefaultValue("") @QueryParam("login") String customerLogin) {
        try {
            List<CustomerPojo> customers = MainFactory.getInstance()
                    .getCustomerManager()
                    .getCustomers().stream()
                    .filter(x -> customerLogin.isEmpty() || x.login.equals(customerLogin))
                    .collect(Collectors.toList());

            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            List<CustomerPojo> customers = MainFactory
                    .getInstance()
                    .getCustomerManager()
                    .getCustomers()
                    .stream()
                    .filter(x -> x.login.equals(customerLogin))
                    .collect(Collectors.toList());

            Validate.isTrue(customers.size() == 1);

            return Response.ok().entity(JsonMapper.toJson(customers.get(0), true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);
            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().createCustomer(customerData);

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @POST
    @Path("/update_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCustomer(String customerDataJson) {
        try {
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);
            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().updateCustomer(customerData);

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @DELETE
    @Path("/delete_customer/{customer_login}")
    public Response deleteCustomer(@PathParam("customer_login") String customerLogin) {
        try {
            List<CustomerPojo> customers = MainFactory
                    .getInstance()
                    .getCustomerManager()
                    .getCustomers()
                    .stream()
                    .filter(x -> x.login.equals(customerLogin))
                    .collect(Collectors.toList());

            Validate.isTrue(customers.size() == 1);

            MainFactory.getInstance().getCustomerManager().deleteCustomer(customers.get(0).id);

            // send the answer
            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.CUSTOMER_ROLE, Globals.ADMIN_ROLE})
    @GET
    @Path("/get_plans")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        try {
            List<PlanPojo> plans = MainFactory.getInstance().getPlanManager().getPlans();
            return Response.ok(JsonMapper.toJson(plans, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @GET
    @Path("/get_plan_id/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlanId(@PathParam("plan_id") String planId) {
        try {
            List<PlanPojo> plans = MainFactory.getInstance().getPlanManager()
                    .getPlans().stream()
                    .filter(x -> x.id.equals(UUID.fromString(planId)))
                    .collect(Collectors.toList());

            Validate.isTrue(plans.size() == 1);

            return Response.ok().entity(JsonMapper.toJson(plans.get(0), true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @POST
    @Path("/create_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPlan(String planJson) {
        try {
            PlanPojo newPlan = JsonMapper.fromJson(planJson, PlanPojo.class);
            PlanPojo plan = MainFactory.getInstance().getPlanManager().createPlan(newPlan);
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @POST
    @Path("/update_plan/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePlan(@PathParam("plan_id") String planIdString, String planJson) {
        try {
            UUID planId = UUID.fromString(planIdString);
            PlanPojo newPlan = JsonMapper.fromJson(planJson, PlanPojo.class);
            newPlan.id = planId;
            PlanPojo plan = MainFactory.getInstance().getPlanManager().updatePlan(newPlan);
            return Response.ok(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
                    ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE})
    @DELETE
    @Path("/delete_plan/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePlan(@PathParam("plan_id") String planIdString) {
        try {
            UUID planId = UUID.fromString(planIdString);
            MainFactory.getInstance().getPlanManager().deletePlan(planId);
            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
                    ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_customer_subscriptions/{customer_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@PathParam("customer_id") String customerIdString) {
        try {
            UUID customerId = UUID.fromString(customerIdString);
            List<SubscriptionPojo> subscriptions = MainFactory.getInstance().getSubscriptionManager()
                    .getSubscriptions(customerId);

            return Response.ok(JsonMapper.toJson(subscriptions, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.CUSTOMER_ROLE, Globals.ADMIN_ROLE})
    @POST
    @Path("/create_subscription")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createSubscription(String subscriptionJson) {
        try {
            SubscriptionPojo newSubscription = JsonMapper.fromJson(subscriptionJson, SubscriptionPojo.class);
            SubscriptionPojo subscription = MainFactory.getInstance().getSubscriptionManager().createSubscription(newSubscription);
            return Response.ok().entity(JsonMapper.toJson(subscription, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.CUSTOMER_ROLE, Globals.ADMIN_ROLE})
    @DELETE
    @Path("/delete_subscription/{subscription_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteSubscription(@PathParam("subscription_id") String subscriptionIdString) {
        try {
            UUID subscriptionId = UUID.fromString(subscriptionIdString);
            MainFactory.getInstance().getSubscriptionManager().deleteSubscription(subscriptionId);
            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
                    ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}