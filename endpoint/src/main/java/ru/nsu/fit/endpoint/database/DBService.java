package ru.nsu.fit.endpoint.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;
import java.util.ArrayList;

public class DBService implements IDBService {
    // Constants

    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id = '%s'";
    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_SUBSCRIPTIONS = "SELECT * FROM SUBSCRIPTION WHERE customer_id='%s'";
    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id='%s'";


    private static final String UPDATE_CUSTOMER = "UPDATE CUSTOMER SET first_name = '%s', last_name = '%s' WHERE id = '%s'";
    private static final String UPDATE_CUSTOMER_BALANCE = "UPDATE CUSTOMER SET balance = '%s' WHERE id = '%s'";

    private static final String DELETE_CUSTOMER = "DELETE FROM CUSTOMER WHERE id = '%s'";
    private static final String DELETE_CUSTOMERS = "DELETE FROM CUSTOMER";
    private static final String DELETE_PLANS = "DELETE FROM PLAN";
    private static final String DELETE_SUBSCRIPTIONS = "DELETE FROM SUBSCRIPTION";


    private static final Logger logger = LoggerFactory.getLogger(DBService.class);
    private static final Object generalMutex = new Object();
    private Connection connection;

    public static final IDBService INSTANCE = new DBService();

    private DBService() {
        init();
    }

    public static IDBService getInstance() {
        return INSTANCE;
    }

    public List<CustomerPojo> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<CustomerPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    CustomerPojo customerData = new CustomerPojo();

                    customerData.id = UUID.fromString(rs.getString(1));
                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo getCustomer(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("getCustomer " + customerId);
            try {
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT id, first_name, last_name, login, pass, balance FROM CUSTOMER WHERE id = ?");
                statement.setString(1, customerId.toString());
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    CustomerPojo customer = new CustomerPojo();
                    customer.id = customerId;
                    customer.firstName = resultSet.getString(2);
                    customer.lastName = resultSet.getString(3);
                    customer.login =resultSet.getString(4);
                    customer.pass = resultSet.getString(5);
                    customer.balance = resultSet.getInt(6);
                    return customer;
                } else {
                    throw new IllegalArgumentException("No customer with id " + customerId);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                }

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
            return null;
        }
    }

    public CustomerPojo createCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));

            customerData.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.id,
                                customerData.firstName,
                                customerData.lastName,
                                customerData.login,
                                customerData.pass,
                                customerData.balance));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo updateCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            logger.info("Method 'updateCustomer' was called with data: " + customerData);

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(
                        UPDATE_CUSTOMER,
                        customerData.firstName,
                        customerData.lastName,
                        customerData.id));
                return customerData;
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public CustomerPojo updateCustomerBalance(UUID id, int amount) {
        synchronized (generalMutex) {
            logger.info("Method 'updateCustomerBalance' was called with UUID: " + id + ", amount: " + amount);

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(
                        UPDATE_CUSTOMER_BALANCE,
                        amount,
                        id
                ));
                ResultSet rs = statement.executeQuery(String.format(
                        SELECT_CUSTOMER_BY_ID,
                        id
                ));
                if (rs.next()) {
                    return new CustomerPojo()
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(rs.getInt(6));
                }
                return null;
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteCustomer(UUID id) {
        synchronized (generalMutex) {
            logger.info("Method 'deleteCustomer' was called with UUID: " + id);

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(
                        DELETE_CUSTOMER,
                        id
                ));
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteAllCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'deleteAllCustomers' was called");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DELETE_CUSTOMERS);
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    private ResultSet getPlanResultSet(UUID id) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement(
                        "SELECT id, name, details, fee FROM PLAN WHERE id = ?",
                        ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_UPDATABLE);
        preparedStatement.setString(1, id.toString());
        return preparedStatement.executeQuery();
    }

    public List<PlanPojo> getPlans() {
        synchronized (generalMutex) {
            this.logger.info("Method 'getPlans' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                List<PlanPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    PlanPojo plan = new PlanPojo();
                    plan.id = UUID.fromString(rs.getString(1));
                    plan.name = rs.getString(2);
                    plan.details = rs.getString(3);
                    plan.fee = rs.getInt(4);

                    result.add(plan);
                }
                return result;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo getPlan(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getPlan' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_ID,
                                id));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Plan with id '" + id + " was not found");
                }

                PlanPojo planData = new PlanPojo();
                planData.id = id;
                planData.name = rs.getString(2);
                planData.details = rs.getString(3);
                planData.fee = rs.getInt(4);
                return planData;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo createPlan(PlanPojo planData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", planData));

            planData.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                planData.id,
                                planData.name,
                                planData.details,
                                planData.fee));
                return planData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo updatePlan(PlanPojo planData) {
        synchronized (generalMutex) {
            logger.info("updatePlan " + planData.id);
            try {
                ResultSet resultSet = getPlanResultSet(planData.id);
                if (resultSet.next()) {
                    resultSet.updateString(2, planData.name);
                    resultSet.updateString(3, planData.details);
                    resultSet.updateInt(4, planData.fee);
                    resultSet.updateRow();
                    return planData;
                } else {
                    throw new IllegalArgumentException("No plan with id " + planData.id);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public void deletePlan(UUID id) {
        synchronized (generalMutex) {
            logger.info("deletePlan " + id);
            try {
                PreparedStatement statement = connection.prepareStatement(
                        "DELETE FROM PLAN WHERE id = ?");
                statement.setString(1, id.toString());
                int deletedPlans = statement.executeUpdate();
                if (deletedPlans == 0) {
                    throw new IllegalArgumentException("No plan with id " + id);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteAllPlans() {
        synchronized (generalMutex) {
            logger.info("Method 'deleteAllPlans' was called");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DELETE_PLANS);
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public List<SubscriptionPojo> getCustomerSubscriptions(UUID customerId) {
        synchronized (generalMutex) {
            this.logger.info("Method 'getSubscriptions' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTIONS,
                                customerId));

                List<SubscriptionPojo> result = Lists.newArrayList();

                while (rs.next()) {
                    SubscriptionPojo subscription = new SubscriptionPojo();
                    subscription.id = UUID.fromString(rs.getString(1));
                    subscription.customerId = UUID.fromString(rs.getString(2));
                    subscription.planId = UUID.fromString(rs.getString(3));

                    PlanPojo plan = getPlan(subscription.planId);

                    subscription.planName = plan.name;
                    subscription.planFee = plan.fee;

                    result.add(subscription);
                }
                return result;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public SubscriptionPojo getSubscription(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getSubscriptionByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTION_BY_ID,
                                id));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Subscription with id '" + id + " was not found");
                }

                SubscriptionPojo subscription = new SubscriptionPojo();
                subscription.customerId = UUID.fromString(rs.getString(2));
                subscription.planId = UUID.fromString(rs.getString(3));
                return subscription;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public SubscriptionPojo createSubscription(SubscriptionPojo subscription, int newBalance) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'createSubscription' was called with data '%s'.", JsonMapper.toJson(subscription, true)));
            subscription.id = UUID.randomUUID();
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_SUBSCRIPTION,
                                subscription.id,
                                subscription.customerId,
                                subscription.planId));

                statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER_BALANCE,
                                newBalance,
                                subscription.customerId));
                return subscription;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void deleteSubscription(UUID id) {
        synchronized (generalMutex) {
            logger.info("deleteSubscription " + id);
            try {
                PreparedStatement statement = connection.prepareStatement(
                        "DELETE FROM SUBSCRIPTION WHERE id = ?");
                statement.setString(1, id.toString());
                int deleteSubscription = statement.executeUpdate();
                if (deleteSubscription == 0) {
                    throw new IllegalArgumentException("No subscription with id " + id);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteAllSubscriptions() {
        synchronized (generalMutex) {
            logger.info("Method 'deleteAllSubscriptions' was called");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DELETE_SUBSCRIPTIONS);
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
