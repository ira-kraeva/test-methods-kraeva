package ru.nsu.fit.endpoint.database;

import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

import java.util.List;
import java.util.UUID;

public interface IDBService {

    List<CustomerPojo> getCustomers();
    CustomerPojo getCustomer(UUID customerId);
    UUID getCustomerIdByLogin(String customerLogin);


    CustomerPojo createCustomer(CustomerPojo customerData);
    CustomerPojo updateCustomer(CustomerPojo customerData);
    CustomerPojo updateCustomerBalance(UUID id, int amount);

    void deleteCustomer(UUID id);
    void deleteAllCustomers();

    List<PlanPojo> getPlans();
    PlanPojo getPlan(UUID id);

    PlanPojo createPlan(PlanPojo planData);
    PlanPojo updatePlan(PlanPojo planData);

    void deletePlan(UUID id);
    void deleteAllPlans();

    List<SubscriptionPojo> getCustomerSubscriptions(UUID customerId);
    SubscriptionPojo getSubscription(UUID id);

    SubscriptionPojo createSubscription(SubscriptionPojo subscription, int newBalance);

    void deleteSubscription(UUID id);
    void deleteAllSubscriptions();

}
