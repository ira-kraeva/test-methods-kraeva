package ru.nsu.fit.endpoint.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriptionPojo {
    @JsonProperty("id")
    public UUID id;

    @JsonProperty("customerId")
    public UUID customerId;

    @JsonProperty("planId")
    public UUID planId;

    @JsonProperty("planName")
    public String planName;

    @JsonProperty("planFee")
    public int planFee;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionPojo subscription = (SubscriptionPojo) o;

        return id != null ? id.equals(subscription.id) : subscription.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
