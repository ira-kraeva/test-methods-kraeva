package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

import java.util.List;
import java.util.UUID;

public class SubscriptionManager extends ParentManager {
    public SubscriptionManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает подписку. Ограничения:
     * 1. Подписки с таким планом пользователь не имеет.
     * 2. Стоймость подписки не превышает текущего баланса кастомера и после покупки вычитается из его баласа.
     */
    public SubscriptionPojo createSubscription(SubscriptionPojo subscription) {
        Validate.notNull(subscription, "Argument 'subscription' is null");
        Validate.notNull(subscription.customerId, "Argument 'subscription.customerId' is null");
        Validate.notNull(subscription.planId, "Argument 'subscription.planId' is null");

        boolean subscriptionUnique = true;
        try {
            this.dbService.getSubscription(subscription.id);
            subscriptionUnique = false;
        } catch (IllegalArgumentException ignored) {
        }
        Validate.isTrue(subscriptionUnique, "There's already such subscription");

        CustomerPojo theCustomer = this.dbService.getCustomer(subscription.customerId);
        PlanPojo thePlan = this.dbService.getPlan(subscription.planId);
        Validate.isTrue(theCustomer.balance <= thePlan.fee, "Customer has not enough money for the plan");

        return this.dbService.createSubscription(subscription, theCustomer.balance - thePlan.fee);
    }

    public void deleteSubscription(UUID subscriptionId) {
        Validate.notNull(subscriptionId, "Argument 'subscriptionId' is null");
        this.dbService.deleteSubscription(subscriptionId);
    }

    /**
     * Возвращает список подписок указанного customer'а.
     */
    public List<SubscriptionPojo> getSubscriptions(UUID customerId) {
        return this.dbService.getCustomerSubscriptions(customerId);
    }
}
