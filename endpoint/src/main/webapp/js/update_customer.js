$(document).ready(function () {

    var email = $.parseUrl(decodeURIComponent(window.location.href)).params.email;
    var customerId;

    $.get({
        url: 'rest/get_customer_id/' + email,
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'password')
        }
    }).done(function (data) {

        customerId = data.id;

        document.getElementById('first_name_id').value = data.firstName;
        document.getElementById('last_name_id').value = data.lastName;
        // document.forms[0].submit()
    });


    $("#update_customer_id").click(
        function () {
            var fName = $("#first_name_id").val();
            var lName = $("#last_name_id").val();

            $.get({
                url: 'rest/update_customer',
                method: 'post',
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'password'),
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    "id": customerId,
                    "firstName": fName,
                    "lastName": lName
                })
            }).done(function () {
                $.redirect('/endpoint/customers.html', 'GET');
            });
        }
    );
});