$(document).ready(function(){
    $("#add_new_customer").click(function() {
        $.redirect('/endpoint/add_customer.html', 'GET');
    });

    $.get({
        url: 'rest/get_customers',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        var dataObj = $.parseJSON(data);
        var dataSet = [];
        for(var i = 0; i < dataObj.length; i++) {
            var obj = dataObj[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.pass, obj.balance])
        }
        console.log(dataSet);
        $('#customer_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "Fist Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Pass" },
                    { title: "Balance" }
                ]
            });
        $('#customer_list_id tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });
    });

    $('#update_customer').click(function () {
        var email = $('#customer_list_id tr.selected').find('td:nth-child(3)').text();
        $.redirect('/endpoint/update_customer.html', {'email': email}, 'GET');

        $('#subscription_list_id tr.selected').remove();
    })

    $('#delete_customer').click(function () {
        var customerId = $('#customer_list_id tr.selected').find('td:nth-child(3)').text();

        $.get({
            url: 'rest/delete_customer/' + customerId,
            method: 'delete',
            headers: {
                'Authorization': 'Basic ' + btoa('admin' + ':' + 'password')
            }
        });

        $('#customer_list_id tr.selected').remove();
    })
});