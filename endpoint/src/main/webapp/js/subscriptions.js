$(document).ready(function () {
    var customerId = $.parseUrl(decodeURIComponent(window.location.href)).params.customerId;
    console.log(customerId);

    $("#add_new_subscription").click(function () {
        $.redirect('/endpoint/add_subscription.html', {'customerId': customerId}, 'GET');
    });

    $.get({
        url: 'rest/get_customer_subscriptions/' + customerId,
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'password')
        }
    }).done(function (data) {
        var dataSet = [];
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            dataSet.push([obj.planName, obj.planFee, obj.id])
        }
        $('#subscription_list_id').DataTable({
            data: dataSet,
            columns: [
                {title: "Plan Name"},
                {title: "Fee"},
                {title: "ID"}
            ]
        });
        $('#subscription_list_id tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });
    });

    $('#delete_subscription').click(function () {
        var sId = $('#subscription_list_id tr.selected').find('td:nth-child(3)').text();

        $.get({
            url: 'rest/delete_subscription/' + sId,
            method: 'delete',
            headers: {
                'Authorization': 'Basic ' + btoa('admin' + ':' + 'password')
            }
        });

        $('#subscription_list_id tr.selected').remove();
    })
});