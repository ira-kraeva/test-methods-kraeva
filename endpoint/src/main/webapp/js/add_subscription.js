$(document).ready(function () {
    var customerId = $.parseUrl(decodeURIComponent(window.location.href)).params.customerId;

    $.get({
        url: 'rest/get_plans',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'password')
        }
    }).done(function (data) {
        var sel = $('#plan_name_id');
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            sel.prepend($("<option>").attr('value', item.id).text(item.name));
        }
    });

    $("#create_subscription_id").click(function () {
            var planId = $("#plan_name_id").val();

            $.get({
                url: 'rest/create_subscription',
                method: 'post',
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'password'),
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    "customerId": customerId,
                    "planId": planId
                })
            }).done(function (data) {
                $.redirect('/endpoint/subscriptions.html', {
                    'customerId': customerId
                }, 'GET');
            });
        }
    );
});